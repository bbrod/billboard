package main

import "testing"

func TestLenText(t *testing.T) {
	if LenText([]string{"Can you hack"}) != 12 {
		t.Error("text len computation return  bad values")
	}
}

func TestFitText(t *testing.T) {
	for _, item := range []*struct {
		width, height int
		text          []string
		expectedMesh  int
	}{
		{20, 6, []string{"hacker", "cup"}, 3},
		{100, 20, []string{"hacker", "cup", "2013"}, 10},
		{10, 20, []string{"MUST", "BE", "ABLE", "TO", "HACK"}, 2},
		{55, 25, []string{"Can", "you", "hack"}, 8},
		{100, 20, []string{"Hack", "your", "way", "to", "the", "cup"}, 7},
	} {
		mesh := FitText(item.width, item.height, item.text)
		if mesh != item.expectedMesh {
			t.Error("fit text fail to compute for item ", item)
		}
	}
}
