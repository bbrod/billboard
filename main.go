package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"sync"
)

const (
	minRecords = 1
	maxRecords = 20

	maxSize = 1000
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	if !scanner.Scan() {
		log.Fatal("cannot process inputs")
	}
	T, err := strconv.Atoi(scanner.Text())
	if err != nil {
		log.Fatal("cannot determine number of test cases")
	}
	if T > maxRecords || T < minRecords {
		log.Fatal("unexpected records number =" + strconv.Itoa(T))
	}
	var lineCount int
	// Create a computation group to process the test cases concurrently
	var computationGroup sync.WaitGroup
	defer computationGroup.Wait()
	for lineCount = 1; scanner.Scan() && T >= lineCount; lineCount++ {
		if err := scanner.Err(); err != nil {
			fmt.Fprint(os.Stderr, "scanning input has experienced troubles: "+err.Error())
			continue
		}
		lineContent := scanner.Text()
		tokens := strings.Split(lineContent, " ")
		if numToken := len(tokens); numToken < 3 {
			fmt.Fprint(os.Stderr, "unexpected input number of token = ", numToken, "\n")
			continue
		}
		W, err := strconv.Atoi(tokens[0])
		if err != nil {
			fmt.Fprint(os.Stderr, "line ", lineCount, " - unexpected format: first token must be an int, got ", tokens[0])
			continue
		}
		H, err := strconv.Atoi(tokens[1])
		if err != nil {
			fmt.Fprint(os.Stderr, "line ", lineCount, " - unexpected format: second token must be an int, got ", tokens[1])
			continue
		}
		S := tokens[2:]
		// compute the test cases
		computationGroup.Add(1)
		go func(W, H int, S []string, caseNum int) {
			defer computationGroup.Done()
			fmt.Printf("case #%d: %d\n", caseNum, FitText(W, H, S))
		}(W, H, S, lineCount)
	}
}

// LenText returns the number of characteres added to the number
// of separations between the words.
func LenText(tokens []string) (l int) {
	// Sum the number of characters contained by the text characters
	for i := range tokens {
		l += len(tokens[i])
	}
	// Add the number of adjascent separations between the words
	nbSeparation := len(tokens) - 1
	l += nbSeparation
	return
}

// FitText takes the width and height and a text. Then it computes the
// maximal font size to use for making the displaying of the constrainted text.
// The function returns 0 if the constraint cannot be satisfied.
func FitText(width, height int, text []string) (mesh int) {
	// The maximalGridMesh equals the number of characteres that are constituing
	// the words of the text to which we the number of adjascent word separation (a space or a newline)..
	maximalGridMesh := width * height / LenText(text)
	// Then iterate over the mesh size to find the biggest possible number
	// The biggest the mesh is the most space is taken by one letter.
	// Hence, maximizing the mesh.
	for mesh = maximalGridMesh; mesh >= 0; mesh-- {
		wMesh := int(width / mesh)
		hMesh := int(height / mesh)
		if textIsFitting(text, wMesh, hMesh) {
			return
		}
	}
	return
}

// textIsFitting returns wether a text message can fit inside
// a grid mesh without splitting any of its words.
func textIsFitting(text []string, wMesh, hMesh int) bool {
	var xCursor, yCursor int
	for _, word := range text {
		wordLen := len(word)
		if wordLen > wMesh {
			// this word is too long to be displayed in one piece
			return false
		}
		if xCursor+wordLen > wMesh {
			yCursor++
			if yCursor >= hMesh {
				// no more line are available to render the current word
				return false
			}
			xCursor = 0
		}
		xCursor += wordLen + 1
	}
	return true
}
